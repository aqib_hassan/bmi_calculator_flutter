import 'package:bmi_calculator_flutter/components/constants.dart';
import 'package:flutter/material.dart';

class CardContent extends StatelessWidget {
  const CardContent({
    super.key,
    required this.label,
    required this.icon,
  });

  final IconData icon;
  final String label;

  @override
  Widget build(BuildContext context) {
    return Column(mainAxisAlignment: MainAxisAlignment.center, children: [
      Icon(
        icon,
        size: 80.0,
      ),
      const SizedBox(height: 15.0),
      Text(
        label,
        style: labelTextStyle,
      )
    ]);
  }
}
