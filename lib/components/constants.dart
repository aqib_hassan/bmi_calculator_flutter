import 'package:flutter/material.dart';

const labelTextStyle = TextStyle(
  fontSize: 18.0,
  color: Color(0xFF8D8E98),
);

const Color activeCardColor = Color(0xFF1D1E33);
const Color bottomButtonColor = Color(0xFFEB1555);
const Color inactiveCardColor = Color(0xFF111328);

enum Gender { male, female, none }
