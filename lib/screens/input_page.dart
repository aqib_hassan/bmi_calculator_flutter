import 'package:bmi_calculator_flutter/calculator_brain.dart';
import 'package:bmi_calculator_flutter/components/bottom_button.dart';
import 'package:bmi_calculator_flutter/components/card_content.dart';
import 'package:bmi_calculator_flutter/components/constants.dart';
import 'package:bmi_calculator_flutter/components/reuseable_card.dart';
import 'package:bmi_calculator_flutter/components/round_icon_button.dart';
import 'package:bmi_calculator_flutter/screens/results_page.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class InputPage extends StatefulWidget {
  const InputPage({super.key});

  @override
  // ignore: library_private_types_in_public_api
  _InputPageState createState() => _InputPageState();
}

class _InputPageState extends State<InputPage> {
  Color maleCardColor = activeCardColor;
  Color femaleCardColor = activeCardColor;

  Gender selectedGender = Gender.none;

  int sliderHeight = 180;
  int bodyWeight = 60;
  int age = 25;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('BMI CALCULATOR'),
      ),
      body: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
        Expanded(
          child: Row(
            children: [
              Expanded(
                child: ReuseableCard(
                  onPress: () {
                    setState(() {
                      selectedGender = Gender.male;
                    });
                  },
                  color: selectedGender == Gender.male
                      ? inactiveCardColor
                      : activeCardColor,
                  cardChild: const CardContent(
                    label: 'MALE',
                    icon: FontAwesomeIcons.mars,
                  ),
                ),
              ),
              Expanded(
                child: ReuseableCard(
                  onPress: () {
                    setState(() {
                      selectedGender = Gender.female;
                    });
                  },
                  color: selectedGender == Gender.female
                      ? inactiveCardColor
                      : activeCardColor,
                  cardChild: const CardContent(
                    label: 'FEMALE',
                    icon: FontAwesomeIcons.venus,
                  ),
                ),
              ),
            ],
          ),
        ),
        Expanded(
          child: ReuseableCard(
            color: activeCardColor,
            cardChild:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: [
              const Text(
                'HEIGHT',
                style: labelTextStyle,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.baseline,
                textBaseline: TextBaseline.alphabetic,
                children: [
                  Text(
                    sliderHeight.toString(),
                    style: labelTextStyle.copyWith(
                        fontSize: 50.0, fontWeight: FontWeight.w900),
                  ),
                  const SizedBox(
                    width: 5.0,
                  ),
                  Text(
                    'cm',
                    style: labelTextStyle.copyWith(),
                  )
                ],
              ),
              SliderTheme(
                data: SliderTheme.of(context).copyWith(
                  overlayColor: Colors.white,
                  thumbShape:
                      const RoundSliderThumbShape(enabledThumbRadius: 15.0),
                  overlayShape:
                      const RoundSliderOverlayShape(overlayRadius: 30.0),
                ),
                child: Slider(
                    value: sliderHeight.toDouble(),
                    min: 120.0,
                    max: 220.0,
                    activeColor: bottomButtonColor,
                    inactiveColor: labelTextStyle.color,
                    onChanged: (double newValue) {
                      setState(() {
                        sliderHeight = newValue.round();
                      });
                    }),
              )
            ]),
            onPress: () {},
          ),
        ),
        Expanded(
          child: Row(
            children: [
              Expanded(
                child: ReuseableCard(
                  color: activeCardColor,
                  cardChild: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text(
                        'WEIGHT',
                        style: labelTextStyle,
                      ),
                      Text(
                        bodyWeight.toString(),
                        style: labelTextStyle.copyWith(
                          fontSize: 50.0,
                          fontWeight: FontWeight.w900,
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          RoundIconButton(
                            icon: FontAwesomeIcons.minus,
                            onPress: () {
                              setState(() {
                                bodyWeight = bodyWeight - 1;
                              });
                            },
                          ),
                          const SizedBox(
                            width: 10.0,
                          ),
                          RoundIconButton(
                            icon: FontAwesomeIcons.plus,
                            onPress: () {
                              setState(() {
                                bodyWeight = bodyWeight + 1;
                              });
                            },
                          ),
                        ],
                      )
                    ],
                  ),
                  onPress: () {},
                ),
              ),
              Expanded(
                child: ReuseableCard(
                  color: activeCardColor,
                  cardChild: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text(
                        'Age',
                        style: labelTextStyle,
                      ),
                      Text(
                        age.toString(),
                        style: labelTextStyle.copyWith(
                          fontSize: 50.0,
                          fontWeight: FontWeight.w900,
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          RoundIconButton(
                            icon: FontAwesomeIcons.minus,
                            onPress: () {
                              setState(() {
                                age = age - 1;
                              });
                            },
                          ),
                          const SizedBox(
                            width: 10.0,
                          ),
                          RoundIconButton(
                            icon: FontAwesomeIcons.plus,
                            onPress: () {
                              setState(() {
                                age = age + 1;
                              });
                            },
                          ),
                        ],
                      )
                    ],
                  ),
                  onPress: () {},
                ),
              ),
            ],
          ),
        ),
        BottomButton(
          text: 'Calculate',
          onPress: () {
            CalculatorBrain calc = CalculatorBrain(sliderHeight, bodyWeight);
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return ResultsPage(
                bmiResult: calc.calculateBMI(),
                resultText: calc.getResult(),
                interpretation: calc.getInterpretation(),
              );
            }));
          },
        ),
      ]),
    );
  }
}
